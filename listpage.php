<?php
include 'connection.php';
?>
<!DOCTYPE html>
<head>
<meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE-edge">
  <meta name="viewport" content="width=device width, initial-scale=1">
  <title>table</title>

  <!--Roboto condensed font-->
  <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:300,300i,400,400i,700,700i" rel="stylesheet">

  <!--Bootstrap CSS-->
  <link rel="stylesheet" type="text/css" href="bootstrap.min.css">
  <!--External stylesheet-->
  <link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
<div class="listsitem">
<table class="table table-striped">
<thead class="thead-dark">
  <tr>
    <th scope="col">Id</th>
    <th scope="col">Address</th>
    <th scope="col">Gender</th>
    <th scope="col">Phone</th>
    <th scope="col">Email</th>
    <th scope="col">Username</th>
    <th scope="col">Password</th>
    <th scope="col">Action</th>
  </tr>
  </thead>

<?php
$stmt=$conn->prepare("SELECT * FROM studentinfo");
$stmt->execute();
$result=$stmt->fetchAll();
foreach ($result as $row) {
?>

  <tr>
    <td><?=$row['id'];?></td>
    <td><?=$row['address'];?></td>
    <td><?=$row['gender'];?></td>
    <td><?=$row['phone'];?></td>
    <td><?=$row['email'];?></td>
    <td><?=$row['username'];?></td>
    <td><?=$row['passwoed'];?></td>
    <td>
       <a href="edit.php?id=<?=$row['id'];?>">Edit</a>
        <a href="delete.php?id=<?=$row['id'];?>">Delete</a>
    </td>
  </tr>
  <?php
}
  ?>
</table>
<a href="insert.php">Home</a>
</div>
</body>