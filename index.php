<!DOCTYPE html>
<html lang="en">

<head>
    <!--Meta tags-->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE-edge">
    <meta name="viewport" content="width=device width, initial-scale=1">
    <title>8Square</title>

    <!--Roboto condensed font-->
    <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:300,300i,400,400i,700,700i" rel="stylesheet">

    <!--Bootstrap CSS-->
    <link rel="stylesheet" type="text/css" href="bootstrap.min.css">

    <!--External stylesheet-->
    <link rel="stylesheet" type="text/css" href="style.css">

</head>

<body>
    <div class="header">
        <h1>Student Management System database </h1>
    </div>

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-4">
                <div class="section1st">
                    <div class="lists">
                        <h4>Options</h4>
                        <div class="container">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="alllist">
                                        <a href="#">Home</a>
                                        <a href="listpage.php">Student list</a>
                                        <a href="#teacher">Teacher</a>
                                        <a href="#course">Course</a>
                                        <a href="#department">Department</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-8">
                <div class="section2nd">
                    <div class="formpart">


                    <h2>Form:</h2>
                    <form action="" method="POST">
                        <table>
                            <tr>
                                <td>Id:</td>
                                <td><input type="text" name="id" placeholder="Enter your id"></td>
                            </tr>

                            <tr>
                                <td>Address:</td>
                                <td><input type="text" name="address" placeholder="Enter your address"></td>
                            </tr>

                            <tr>
                                <td>Gender:</td>
                                <td><input type="radio" name="gender" value="Male">Male</td>
                                <td><input type="radio" name="gender" value="Female">Female</td>
                            </tr>

                            <tr>
                                <td>Phone:</td>
                                <td><input type="tel" name="phone" placeholder="Enter your phone number"></td>
                            </tr>

                            <tr>
                                <td>Email:</td>
                                <td><input type="email" name="email" placeholder="example@example.com"></td>
                            </tr>

                            <tr>
                                <td>Username:</td>
                                <td><input type="text" name="username" placeholder="Enter username"></td>
                            </tr>

                            <tr>
                                <td>Password:</td>
                                <td><input type="Password" name="Passwoed" placeholder="Enter your Password"></td>
                            </tr>

                            <tr>
                                <td><input type="submit" name="submit" value="SignUp"></td>
                            </tr>

                        </table>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="footer">
        <p>&copy; Copyright 2019 schoolmanagement</p>
    </div>
</body>