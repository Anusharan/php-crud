<?php
        include ('connection.php');
        if (isset($_POST['submit'])){
            $id=$_POST['id'];
            $address= $_POST['address'];
            $gender= $_POST['gender'];
            $phone= $_POST['phone'];
            $email=$_POST['email'];
            $username= $_POST['username'];
            $passwoed=$_POST['passwoed'];
            if (!empty($username)){
                try{


                $edit= $conn->prepare("UPDATE studentinfo SET address= :address,gender= :gender,phone= :phone,email= :email,username= :username,passwoed= :passwoed WHERE id= :id"); 
    
                $edit->bindParam(':id',$id);
                $edit->bindParam(':address',$address);
                $edit->bindParam(':gender', $gender);
                $edit->bindParam(':phone', $phone);
                $edit->bindParam(':email', $email);
                $edit->bindParam(':username', $username);
                $edit->bindParam(':passwoed', $passwoed);
                $edit->execute();
                echo"Inserted successfully";
                if($edit){
                header('Location:insert.php');
            }
            }
            catch (PDOException $e){
                echo $e->getMessage();
            }
        }
            else{
                echo "Please fill all fields";
            }
           
        }

        $id=0;
        $address="";
        $gender="";
        $phone="";
        $email="";
        $username="";
        $passwoed="";
        if (isset($_GET['id'])) {
            $id=$_GET['id'];
            $insert=$conn->prepare('SELECT * FROM studentinfo WHERE id= :id');
            $insert->execute(array(':id'=>$id));
            $row=$insert->fetch();
            $id=$row['id'];
            $address=$row['address'];
            $gender=$row['gender'];
            $phone=$row['phone'];
            $email=$row['email'];
            $username=$row['username'];
            $passwoed=$row['passwoed'];
        }

        ?>

        <!DOCTYPE html>
<html lang="en">

<head>
    <!--Meta tags-->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE-edge">
    <meta name="viewport" content="width=device width, initial-scale=1">
    <title>Edit</title>

    <!--Roboto condensed font-->
    <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:300,300i,400,400i,700,700i" rel="stylesheet">

    <!--Bootstrap CSS-->
    <link rel="stylesheet" type="text/css" href="bootstrap.min.css">

    <!--External stylesheet-->
    <link rel="stylesheet" type="text/css" href="style.css">

</head>

<body>
    </div>
    
    <div class="header">
        <h1>Student Management System database </h1>
    </div>

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-4">
                <div class="section1st">
                    <div class="lists">
                        <h4>Options</h4>
                        <div class="container">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="alllist">
                                        <a href="#">Home</a>
                                        <a href="listpage.php">Student list</a>
                                        <a href="#teacher">Teacher</a>
                                        <a href="#course">Course</a>
                                        <a href="#department">Department</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-8">
                <div class="section2nd">
                    <div class="formpart">


                    <h2>Form:</h2>
                    <form action="" method="POST">
                        <table>
                            <tr>
                                <td>Id:</td>
                                <td><input type="text" name="id" value="<?=$id;?>"></td>
                            </tr>

                            <tr>
                                <td>Address:</td>
                                <td><input type="text" name="address"
                                value="<?=$address;?>"></td>
                            </tr>

                            <tr>
                                <td>Gender:</td>
                                <td><input type="radio" name="gender" value=>Male</td>
                                <td><input type="radio" name="gender" value="Female">Female</td>
                            </tr>

                            <tr>
                                <td>Phone:</td>
                                <td><input type="tel" name="phone"
                                value="<?=$phone;?>"></td>
                            </tr>

                            <tr>
                                <td>Email:</td>
                                <td><input type="email" name="email" value="<?=$email;?>"></td>
                            </tr>

                            <tr>
                                <td>Username:</td>
                                <td><input type="text" name="username" value="<?=$username;?>"></td>
                            </tr>

                            <tr>
                                <td>Password:</td>
                                <td><input type="Password" name="Passwoed"
                                 value="<?=$passwoed;?>"></td>
                            </tr>

                            <tr>
                                <td><input type="submit" name="submit" value= "SignUp"></td>
                            </tr>

                        </table>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="footer">
        <p>&copy; Copyright 2019 schoolmanagement</p>
    </div>
</body>