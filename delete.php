<?php
include 'connection.php';
include 'insert.php';

if(isset($_GET['id'])){
    $id=$_GET['id'];
    try{
        $delete=$conn->prepare("DELETE FROM studentinfo WHERE id=?");
        $delete->execute(array($id));
        header('Location:listpage.php');
    }
    catch(PDOException $e){
        echo $e->message();
    }
}
?>